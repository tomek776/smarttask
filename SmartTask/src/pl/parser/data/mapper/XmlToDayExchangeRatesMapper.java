package pl.parser.data.mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import pl.parser.data.model.Currency;
import pl.parser.data.model.DayExchangeRates;
import pl.parser.data.model.ForeignExchangeRate;

public class XmlToDayExchangeRatesMapper {
	private static final String POZYCJA = "pozycja";
	private static final String DATA_PUBLIKACJI = "data_publikacji";
	private static final String DATA_NOTOWANIA = "data_notowania";
	private static final String KURS_KUPNA = "kurs_kupna";

	private static final String KURS_SPRZEDAZY = "kurs_sprzedazy";

	private static final String KOD_WALUTY = "kod_waluty";

	public static DayExchangeRates mapToDayExchangeRates(Document doc) {
		List<ForeignExchangeRate> resultPossitions = new ArrayList<ForeignExchangeRate>();

		String quoteDate = doc.getElementsByTagName(DATA_NOTOWANIA).item(0).getTextContent();
		String publicationDate = doc.getElementsByTagName(DATA_PUBLIKACJI).item(0).getTextContent();
		NodeList possitions = doc.getElementsByTagName(POZYCJA);
		for (int i = 0; i < possitions.getLength(); i++) {
			Node nNode = possitions.item(i);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				resultPossitions.add(mapToForeignExchangeRate(eElement));
			}
		}
		return new DayExchangeRates(quoteDate, publicationDate, resultPossitions);
	}

	private static ForeignExchangeRate mapToForeignExchangeRate(Element element) {
		String currency = element.getElementsByTagName(KOD_WALUTY).item(0).getTextContent();
		String askPrice = element.getElementsByTagName(KURS_SPRZEDAZY).item(0).getTextContent();
		String purchasePrice = element.getElementsByTagName(KURS_KUPNA).item(0).getTextContent();
		return new ForeignExchangeRate(Currency.valueOf(currency), parseToBigDecimal(askPrice),
				parseToBigDecimal(purchasePrice));
	}

	private static BigDecimal parseToBigDecimal(String value) {
		return new BigDecimal(value.replaceAll(",", "\\."));
	}
}
