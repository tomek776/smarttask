package pl.parser.data.model;

public enum Currency {
	USD,GBP,CHF,EUR,AUD,CAD,HUF,JPY,CZK,DKK,NOK,SEK,XDR;
	
	public static boolean isEnum(String currency){
		for(Currency cur:Currency.values()){
			if(cur.toString().equals(currency)){
				return true;
			}
		}
		return false;
	}
}
