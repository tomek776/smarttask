package pl.parser.data.model;

import java.math.BigDecimal;

public class ForeignExchangeRate {
	final private Currency currency;
	final private BigDecimal askPrice;
	final private BigDecimal purchasePrice;
	
	
	public Currency getCurrency() {
		return currency;
	}

	public BigDecimal getAskPrice() {
		return askPrice;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public ForeignExchangeRate(Currency currency,BigDecimal askPrice,BigDecimal purchasePrice){
		this.currency= currency;
		this.askPrice= askPrice;
		this.purchasePrice= purchasePrice;
	}

}
