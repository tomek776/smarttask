package pl.parser.data.model;

import java.util.ArrayList;
import java.util.List;

public class DayExchangeRates {
	final private String publicationDate;
	final private String quoteDate;
	final private List<ForeignExchangeRate> resultPossitions;
	
	public DayExchangeRates(String publicationDate,String quoteDate,List<ForeignExchangeRate> resultPossitions){
		this.publicationDate = publicationDate;
		this.quoteDate = quoteDate;
		this.resultPossitions = resultPossitions;
	}
	
	public String getPublicationDate(){
		return publicationDate;
	}
	
	public String getQuoteDate(){
		return quoteDate;
	}

	public List<ForeignExchangeRate> getResultPossitions(){
		return resultPossitions;
	}
	
	public DayExchangeRates filterByCurrency(Currency currency){
		List<ForeignExchangeRate> output = new ArrayList<ForeignExchangeRate>();
		for(ForeignExchangeRate fer:resultPossitions){
			if(currency.equals(fer.getCurrency())){
				output.add(fer);
			}
		}
		return new DayExchangeRates(publicationDate,quoteDate,output);
	}
}
