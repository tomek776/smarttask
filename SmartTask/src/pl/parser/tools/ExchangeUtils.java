package pl.parser.tools;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import pl.parser.data.model.Currency;
import pl.parser.data.model.DayExchangeRates;
import pl.parser.data.model.ForeignExchangeRate;

public class ExchangeUtils {

	private ExchangeUtils() {
	};

	private static List<DayExchangeRates> filterDayExchangesByCurrency(List<DayExchangeRates> dayExchangeDates,
			Currency currency) {
		List<DayExchangeRates> output = new ArrayList<DayExchangeRates>();
		for (DayExchangeRates der : dayExchangeDates) {
			output.add(der.filterByCurrency(currency));
		}
		return output;
	}

	public static BigDecimal getAskMean(List<DayExchangeRates> dayExchangeDates, Currency currency) {
		List<BigDecimal> askPriceList = getAskPriceList(dayExchangeDates, currency);
		return MathHelper.getMean(askPriceList);
	}

	public static BigDecimal getPurchaseMean(List<DayExchangeRates> dayExchangeDates, Currency currency) {
		List<BigDecimal> purchasePriceList = getPurchasePriceList(dayExchangeDates, currency);
		return MathHelper.getMean(purchasePriceList);
	}

	private static List<BigDecimal> getPurchasePriceList(List<DayExchangeRates> dayExchangeDates, Currency currency) {
		List<BigDecimal> purchasePriceList = new ArrayList<BigDecimal>();
		for (DayExchangeRates der : filterDayExchangesByCurrency(dayExchangeDates, currency)) {
			for (ForeignExchangeRate fer : der.getResultPossitions()) {
				purchasePriceList.add(fer.getPurchasePrice());
			}
		}
		return purchasePriceList;
	}

	private static List<BigDecimal> getAskPriceList(List<DayExchangeRates> dayExchangeDates, Currency currency) {
		List<BigDecimal> askPriceList = new ArrayList<BigDecimal>();
		for (DayExchangeRates der : filterDayExchangesByCurrency(dayExchangeDates, currency)) {
			for (ForeignExchangeRate fer : der.getResultPossitions()) {
				askPriceList.add(fer.getAskPrice());
			}
		}
		return askPriceList;
	}

	public static BigDecimal getAskDeviation(List<DayExchangeRates> dayExchangeDates, Currency currency) {
		List<BigDecimal> askPriceList = getAskPriceList(dayExchangeDates, currency);
		return MathHelper.getDeviation(askPriceList);

	}
	
	public static BigDecimal getPurchaseDeviation(List<DayExchangeRates> dayExchangeDates, Currency currency) {
		List<BigDecimal> purchasePriceList = getPurchasePriceList(dayExchangeDates, currency);
		return MathHelper.getDeviation(purchasePriceList);

	}

}
