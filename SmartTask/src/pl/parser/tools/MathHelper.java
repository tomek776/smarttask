package pl.parser.tools;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

public class MathHelper {

	public static BigDecimal getMean(List<BigDecimal> list) {
		BigDecimal sum = new BigDecimal(0.0);
		if (list.size() != 0) {
			for (BigDecimal component : list) {
				sum = sum.add(component);
			}
			return sum.divide(new BigDecimal(list.size()), 5, RoundingMode.HALF_UP)
					.round(new MathContext(5, RoundingMode.HALF_DOWN));
		} else {
			return sum;
		}
	}

	public static BigDecimal getDeviation(List<BigDecimal> list) {
		BigDecimal mean = getMean(list);
		BigDecimal output = new BigDecimal(0.0);
		BigDecimal partialSum = new BigDecimal(0.0);
		for (BigDecimal dec : list) {
			partialSum = partialSum.add(dec.subtract(mean).pow(2));
		}
		if (list.size() > 1) {
			output = output.add(partialSum.divide(new BigDecimal(list.size() - 1), 5, RoundingMode.HALF_UP));
			return sqrt(output).round(new MathContext(3, RoundingMode.HALF_DOWN));
		} else {
			return output;
		}
	}

	/**
	 * This method was enriched by element which increasing accuracy of square
	 * root calculation. {@link BigDecimal}
	 * 
	 * @param value
	 * @return
	 */
	public static BigDecimal sqrt(BigDecimal value) {
		BigDecimal x = new BigDecimal(Math.sqrt(value.doubleValue()));
		return x.add(new BigDecimal(value.subtract(x.multiply(x)).doubleValue() / (x.doubleValue() * 2.0)));
	}

}
