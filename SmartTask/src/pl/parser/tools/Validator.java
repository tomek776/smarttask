package pl.parser.tools;

import java.util.Calendar;
import java.util.Date;

import pl.parser.data.model.CurrencyAllowable;
import pl.parser.tools.exceptions.CurrencyException;
import pl.parser.tools.exceptions.DateFormatException;
import pl.parser.tools.exceptions.DateRangeException;

public class Validator {

	public static void validateCurrency(String currency) throws CurrencyException {
		try {
			CurrencyAllowable.valueOf(currency);
		} catch (IllegalArgumentException e) {
			throw new CurrencyException(currency);
		}

	}

	public static void validateDateRange(String dateFrom, String dateTo)
			throws DateRangeException, DateFormatException {
		Date parsedDateFrom = DateParser.parseDate(dateFrom);
		if (parsedDateFrom == null) {
			throw new DateFormatException(dateFrom);
		}

		Date parsedDateTo = DateParser.parseDate(dateTo);
		if (parsedDateTo == null) {
			throw new DateFormatException(dateTo);
		}

		Calendar calFrom = Calendar.getInstance();
		calFrom.setTime(DateParser.parseDate(dateFrom));
		Calendar calTo = Calendar.getInstance();
		calTo.setTime(DateParser.parseDate(dateTo));
		if (calFrom.after(calTo)) {
			throw new DateRangeException(calFrom.getTime().toString() + "/" + calTo.getTime().toString());
		}
	}

}
