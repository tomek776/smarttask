package pl.parser.tools;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.w3c.dom.Document;

import pl.parser.data.mapper.XmlToDayExchangeRatesMapper;
import pl.parser.data.model.DayExchangeRates;
import pl.parser.tools.exceptions.DateFormatException;
import pl.parser.tools.exceptions.DateRangeException;
import pl.parser.tools.http.HttpClient;

public class NbpRatesExtractor {

	final static String URL = "http://www.nbp.pl/kursy/xml/";

	public List<DayExchangeRates> getNbpRatesFromPeriod(Date dateFrom, Date dateTo)
			throws DateRangeException, DateFormatException, IOException, ParseException {
		List<DayExchangeRates> rates = new ArrayList<>();
		for (Document doc : getDateExchangeFiles(dateFrom, dateTo)) {
			rates.add(XmlToDayExchangeRatesMapper.mapToDayExchangeRates(doc));
		}
		return rates;
	}

	private List<Document> getDateExchangeFiles(Date dateFrom, Date dateTo) throws IOException, ParseException {
		List<String> fileNames = getDateExchangeFileNames(dateFrom, dateTo);
		List<Document> result = new ArrayList<Document>();
		for (String str : fileNames) {
			Document doc = null;
			try {
				String completedUrl = URL + str + ".xml";
				doc = HttpClient.getXmlUriContent(completedUrl);
				FileSaver.saveFile(completedUrl, doc);
			} catch (Exception e) {
				e.printStackTrace();
			}
			result.add(doc);
		}
		return result;
	}

	private List<String> getDateExchangeFileNames(Date dateFrom, Date dateTo) throws IOException, ParseException {

		List<String> result = new ArrayList<String>();
		Calendar calFrom = Calendar.getInstance();
		Calendar calTo = Calendar.getInstance();
		calFrom.setTime(dateFrom);
		calTo.setTime(dateTo);
		int yearFrom = calFrom.get(Calendar.YEAR);
		int yearTo = calTo.get(Calendar.YEAR);
		Calendar calCurr = Calendar.getInstance();
		calCurr.setTime(new Date());
		int currentYear = calCurr.get(Calendar.YEAR);
		while (yearFrom <= yearTo) {
			String urlToBeUsed = getURLOfFileList(yearFrom, currentYear);
			BufferedReader bufReader = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(HttpClient.getByteUriContent(urlToBeUsed))));
			try {
				result = extractFilesBetweenDates(calFrom, calTo, bufReader);
			} catch (IOException e) {
				e.printStackTrace();
			}
			yearFrom++;
		}
		return result;
	}

	private String getURLOfFileList(int yearFrom, int currentYear) {
		String urlToBeUsed;
		if (yearFrom != currentYear) {
			urlToBeUsed = URL + "dir" + yearFrom + ".txt";
		} else {
			urlToBeUsed = URL + "dir.txt";
		}
		return urlToBeUsed;
	}

	private List<String> extractFilesBetweenDates(Calendar calFrom, Calendar calTo, BufferedReader bufReader)
			throws IOException, ParseException {
		List<String> result = new ArrayList<>();
		String line = null;
		SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
		while ((line = bufReader.readLine()) != null) {
			Pattern datePattern = Pattern.compile("c([0-9]{3})z(?<date>[0-9]{6})");
			Matcher matcher = datePattern.matcher(line);
			if (matcher.matches()) {
				Date date = format.parse(matcher.group("date"));
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				if ((calFrom.before(cal) && calTo.after(cal)) || calFrom.equals(cal) || calTo.equals(cal)) {
					result.add(line);
				}
			}
		}
		return result;
	}

}
