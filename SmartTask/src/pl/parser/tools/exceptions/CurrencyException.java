package pl.parser.tools.exceptions;

public class CurrencyException extends Exception {

	private static final long serialVersionUID = 1L;

	public CurrencyException(String message) {
		super("Currency: " + message + " is not allowable");
	}

}
