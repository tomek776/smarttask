package pl.parser.tools.exceptions;

public class DateRangeException extends Exception {

	private static final long serialVersionUID = 1L;

	public DateRangeException(String message) {
		super("Date range: " + message + " is unproper.");
	}

}
