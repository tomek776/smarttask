package pl.parser.tools.exceptions;

public class DateFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public DateFormatException(String message) {
		super("Date " + message + " has invalid format.");
	}

}
