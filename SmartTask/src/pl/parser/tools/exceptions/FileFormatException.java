package pl.parser.tools.exceptions;

public class FileFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public FileFormatException(String message) {
		super("File format: " + message + " is not supported to be saved.");
	}

}
