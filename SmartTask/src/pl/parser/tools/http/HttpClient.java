package pl.parser.tools.http;

import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;

public class HttpClient {

	public static Document getXmlUriContent(String url) throws Exception {
		URL obj = new URL(url);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(obj.openStream());
		return doc;

	}

	public static byte[] getByteUriContent(String url) throws IOException {
		URL obj = new URL(url);
		return IOUtils.toByteArray(obj.openStream());
	}

}
