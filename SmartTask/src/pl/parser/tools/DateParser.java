package pl.parser.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {
	private final static String DEFAULT_FORMAT = "yyyy-MM-dd";

	public static Date parseDate(String date) {	
		try {
			return new SimpleDateFormat(DEFAULT_FORMAT).parse(date);
		} catch (ParseException e) {
			return null;
		}
	}
}