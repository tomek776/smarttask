package pl.parser.nbp.tests;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pl.parser.tools.DateParser;
import pl.parser.tools.FileSaver;
import pl.parser.tools.MathHelper;
import pl.parser.tools.NbpRatesExtractor;
import pl.parser.tools.Validator;
import pl.parser.tools.exceptions.CurrencyException;
import pl.parser.tools.exceptions.DateFormatException;
import pl.parser.tools.exceptions.DateRangeException;

public class TestTools {

	@Test
	public void testDateParser() {
		Assert.assertNotNull(DateParser.parseDate("2016-11-03"));
	}

	@Test
	public void testGetMean() {
		List<BigDecimal> meanComponents = Arrays.asList(new BigDecimal(2.0), new BigDecimal(4.0));
		Assert.assertEquals(3, MathHelper.getMean(meanComponents).intValue());
	}

	@Test
	public void testGetDeviation() {
		List<BigDecimal> meanComponents = Arrays.asList(new BigDecimal(1.0), new BigDecimal(2.0));
		Assert.assertEquals(1, MathHelper.getDeviation(meanComponents).compareTo(new BigDecimal(0.707)));
	}

	@Test
	public void testSqrt() {
		Assert.assertEquals(2, MathHelper.sqrt(new BigDecimal(4.0)).intValue());
	}
	
	@Test
	public void testGetFileNameOfFileList(){
		try{
		Method method = NbpRatesExtractor.class.getDeclaredMethod("getURLOfFileList", int.class, int.class);
		method.setAccessible(true);
		Assert.assertEquals("http://www.nbp.pl/kursy/xml/dir2015.txt", method.invoke(new NbpRatesExtractor(), 2015,2016));
		}catch(NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
			e.printStackTrace();
		}
	}
	
	@Test(expected=CurrencyException.class)
	public void testValidateCurrency() throws CurrencyException{
		Validator.validateCurrency("USDS");
	}
	
	@Test(expected=DateRangeException.class)
	public void testValidateDateRangeBad() throws DateRangeException, DateFormatException{
		Validator.validateDateRange("2016-11-05", "2016-11-03");
	}
	
	@Test
	public void testValidateDateRangeSunshine() throws DateRangeException, DateFormatException{
		Validator.validateDateRange("2016-11-03", "2016-11-03");
	}
	
	@Test
	public void testGetFileName(){
		try{
			Method method = FileSaver.class.getDeclaredMethod("getFileName", String.class);
			method.setAccessible(true);
			Assert.assertEquals("dir2015.txt", method.invoke(new FileSaver(), "http://www.nbp.pl/kursy/xml/dir2015.txt"));
			}catch(NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e){
				e.printStackTrace();
			}
	}
	
	@Test
	public void testGetAskMean(){
		
	}
	
}
