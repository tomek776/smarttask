package pl.parser.nbp;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import pl.parser.data.model.Currency;
import pl.parser.data.model.DayExchangeRates;
import pl.parser.tools.DateParser;
import pl.parser.tools.ExchangeUtils;
import pl.parser.tools.NbpRatesExtractor;
import pl.parser.tools.Validator;
import pl.parser.tools.exceptions.CurrencyException;
import pl.parser.tools.exceptions.DateFormatException;
import pl.parser.tools.exceptions.DateRangeException;

public class MainClass {

	public static void main(String[] args) {
		printMeanAndDeviation("GBP", "2015-02-02", "2015-02-04");
	}

	private static void printMeanAndDeviation(String currencyString, String dateFromString, String dateToString) {
		try {
			Validator.validateDateRange(dateFromString, dateToString);
			Date dateFrom = DateParser.parseDate(dateFromString);
			Date dateTo = DateParser.parseDate(dateToString);

			NbpRatesExtractor extractor = new NbpRatesExtractor();
			List<DayExchangeRates> rates = extractor.getNbpRatesFromPeriod(dateFrom, dateTo);

			Validator.validateCurrency(currencyString);
			Currency currency = Currency.valueOf(currencyString);
			System.out
					.println("Mean ask rate for " + currencyString + ": " + ExchangeUtils.getAskMean(rates, currency));
			System.out.println("Deviation of purchase rate for " + currencyString + ": "
					+ ExchangeUtils.getPurchaseDeviation(rates, currency));
		} catch (DateRangeException | DateFormatException | IOException | CurrencyException | ParseException e) {
			e.printStackTrace();
		}
	}

}
